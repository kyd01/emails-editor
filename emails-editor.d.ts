import { Options } from './src/options.interface';

declare class Component {
    public element: HTMLElement;

    public addClass(className: string);

    public removeClass(className: string);
}

declare class ControlComponent extends Component {
    public addEmail(value: string, silent: boolean);

    public addEmails(values: string[] | string);

    public getEmails(): string[];

    public reset(emails: string[] | string);

    public emailExists(value: string): boolean;

    public removeEmail(value: string);

    public getValidEmailsCount(): number;
}

declare namespace EmailsEditor {
    function create(options: Options): ControlComponent;
}

export = EmailsEditor;
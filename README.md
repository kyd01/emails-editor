# Emails Editor

Component based on pure JavaScript. 
Allows you to edit multiple emails.

## Install

### NPM

```sh
$ npm i -P https://bitbucket.org/kyd01/emails-editor.git
```

### Bower

```sh
$ bower i -S https://bitbucket.org/kyd01/emails-editor.git
```

If troubles, update bower.

```sh
$ npm update -g bower
```

### Inline HTML

```html
<head>
 <script src="CDN_LINK_HERE/emails-editor.js"></script>  
 <link type="text/css" rel="stylesheet" href="CDN_LINK_HERE/emails-editor.css">  
</head>
```

## Using

Initialize component:

```js
const container = document.querySelector('#email-editor');
const options = {
    container
};

EmailsEditor.create(options);
```

### Options

```typescript
interface Options {
    container: Element;
    emails?: string[] | string;
    title?: string;
    titleTagName?: string;
    addMoreText?: string;
    addRandomButtonText?: string;
    showValidCountButtonText?: string;
}
```

#### container

Container for render emails editor component.

#### emails

Pass component emails initial value.

#### title

Component title text. HTML allowed. Default `'Share <strong>Board name</strong> with other'`.

#### titleTagName

Tag name for title HTML element. Default `h2`.

#### addMoreText

New email input placeholder. Default `add more people...`.

#### addRandomButtonText

Text for button with add random email action. Default `Add email`.

#### showValidCountButtonText

Text for button with show valid emails count action. Default `Get email count`.

## Control

Manipulate emails via control.

```js
const control = EmailsEditor.create(options);
```

### Get emails list

```js
control.getEmails();
```

### Reset emails list

```js
const emails = ['new@my.email'];
control.reset(emails);
```

### Listen emails list change

```js
control.element.addEventListener('emailsChange', (e) => console.log(e.detail));
```

### Etc

Also you can use `addEmail(), addEmails(), emailExists(), removeEmail(), getValidEmailsCount()` methods.

## Development

Webpack in watch mode with browser live reload.

```sh
$ npm run-script dev
```

Build:

```sh
$ npm run-script build
```

Typescript linter:

```sh
$ npm run-script lint
```

Styles linter:

```sh
$ npm run-script stylelint
```

Tests:

```sh
$ npm run-script test
```
import { EmailComponent } from './email.component';


describe('EmailComponent', () => {
    let component: EmailComponent;
    let content: HTMLElement;

    beforeEach(() => {
        component = new EmailComponent();
        component.render();
        content = component.element.querySelector<HTMLElement>('.' + EmailComponent.contentClass);
    });

    afterEach(() => {
        component.element.remove();
        component = null;
    });

    it('should be initialized', () => {
        expect(component.element).toBeTruthy();
    });

    it('should get/set value', () => {
        const value = 'test';
        component.value = value;

        expect(component.value).toBe(value);
        expect(component.input.element.value).toBe(value);
        expect(content.innerText).toBe(value);
    });

    it('should validate', () => {
        component.value = 'test@test.test';
        component.validate();
        expect(component.isValid).toBeTruthy();

        component.value = 'test';
        component.validate();
        expect(component.isValid).toBeFalsy();
    });

    it('should activate', () => {
        component.activate();
        expect(component.element.classList.contains(EmailComponent.activeClass)).toBeTruthy();

        component.deactivate();
        expect(component.element.classList.contains(EmailComponent.activeClass)).toBeFalsy();
    });

    it('should pass value to content', () => {
        const value = 'test';
        component.input.element.value = value;
        component.input.element.dispatchEvent(new Event('input'));
        expect(content.innerText).toBe(value);
    });
});
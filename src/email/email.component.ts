import { Component } from '../component/component.class';
import { InputComponent } from '../input/input.component';

require('./email.component.sass');

export class EmailComponent extends Component {
    public static readonly hostClass: string = 'emails-control__email';
    public static readonly contentClass: string = EmailComponent.hostClass + '-content';
    public static readonly inputClass: string = EmailComponent.hostClass + '-input';
    public static readonly removeClass: string = EmailComponent.hostClass + '-remove';
    public static readonly activeClass: string = EmailComponent.hostClass + '--active';
    public static readonly editOnlyClass: string = EmailComponent.hostClass + '--edit-only';
    public static readonly invalidClass: string = EmailComponent.hostClass + '--invalid';

    public get value(): string {
        return this.input.element.value;
    }

    public set value(email: string) {
        this.contentElement.innerText = email;
        this.input.element.value = email;
    }

    public get isValid(): boolean {
        return this.isValidValue;
    }

    public isEditOnly: boolean = false;
    public input: InputComponent;

    private contentElement: HTMLElement;
    private isValidValue: boolean;

    /* tslint:disable */
    private static readonly emailRegExp: RegExp = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
    /* tslint:enable */

    public render() {
        this.addClass(EmailComponent.hostClass);

        this.contentElement = Component.newElement();
        this.contentElement.classList.add(EmailComponent.contentClass);
        this.element.appendChild(this.contentElement);

        this.input = new InputComponent();
        this.input.addClass(EmailComponent.inputClass);
        this.element.appendChild(this.input.element);

        if (this.isEditOnly) {
            this.element.classList.add(EmailComponent.editOnlyClass);
        } else {
            const removeElement = Component.newElement();
            removeElement.classList.add(EmailComponent.removeClass);
            this.element.appendChild(removeElement);
        }

        this.input.element.oninput = this.onInput.bind(this);
    }

    public activate() {
        this.addClass(EmailComponent.activeClass);
    }

    public deactivate() {
        this.input.onInputEnd = null;
        this.input.onPaste = null;
        this.removeClass(EmailComponent.activeClass);
    }

    public validate() {
        this.isValidValue = EmailComponent.emailRegExp.test(this.value);
        this.isValid ? this.removeClass(EmailComponent.invalidClass) : this.addClass(EmailComponent.invalidClass);
    }

    private onInput(event: Event) {
        this.value = (event.target as HTMLInputElement).value;
    }
}
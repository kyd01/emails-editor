import { create } from './index';
import { Options } from './options.interface';
import { EditorComponent } from './editor/editor.component';
import { ControlComponent } from './control/control.component';
import { EmailComponent } from './email/email.component';

describe('EmailsEditor function', () => {
    let container: HTMLElement;

    const createContainer = (): HTMLElement => {
        const container = document.createElement('div');
        document.body.appendChild(container);
        return container;
    };

    beforeEach(() => {
        container = createContainer();
    });

    afterEach(() => {
        if (container) {
            container.remove();
        }
    });

    it('should be initialized', () => {
        create({container});

        expect(container.classList.contains(EditorComponent.hostClass)).toBeTruthy();
    });

    it('should be set options', () => {
        const options: Options = {
            container,
            emails: ['test@test.test', 'test2@test.test', 'test@test', '123'],
            title: '<strong>title</strong>',
            titleTagName: 'h4',
            addMoreText: 'addMoreText',
            addRandomButtonText: 'addRandomButtonText',
            showValidCountButtonText: 'showValidCountButtonText'
        };

        create(options);

        const emails = document.querySelectorAll('.' + EmailComponent.hostClass);
        const emailsValues = Array.from(emails).filter((email) => {
            return !email.classList.contains(ControlComponent.newEmailClass);
        }).map((email) => {
            const content = email.querySelector<HTMLElement>('.' + EmailComponent.contentClass);
            return content.innerText;
        });
        expect(emailsValues).toEqual(options.emails);

        const title =  document.querySelector('.' + EditorComponent.titleClass);
        expect(title.innerHTML).toBe(options.title);
        expect(title.tagName.toLowerCase()).toBe(options.titleTagName);

        const randomButton =  document.querySelector<HTMLElement>('.' + EditorComponent.addRandomClass);
        expect(randomButton.innerText).toBe(options.addRandomButtonText);

        const countButton =  document.querySelector<HTMLElement>('.' + EditorComponent.showValidCountClass);
        expect(countButton.innerText).toBe(options.showValidCountButtonText);

        const newEmail = document.querySelector<HTMLElement>('.' + ControlComponent.newEmailClass);
        const newEmailInput = newEmail.querySelector<HTMLInputElement>('.' + EmailComponent.inputClass);
        expect(newEmailInput.placeholder).toBe(options.addMoreText);
    });

    it('should be initialized several instances', () => {
        const containers = [container, createContainer(), createContainer()];

        containers.forEach((container) => {
            create({container});
        });

        containers.forEach((container) => {
            expect(container.classList.contains(EditorComponent.hostClass)).toBeTruthy();
            container.remove();
        });
    });
});
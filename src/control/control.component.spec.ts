import { ControlComponent } from './control.component';
import { EmailComponent } from '../email/email.component';

describe('ControlComponent', () => {
    let component: ControlComponent;

    const emailValue = 'test@test.test';
    const emails = ['test@test.test', 'test2@test.test', 'test@test', '123'];

    beforeEach(() => {
        component = new ControlComponent();
        component.render();
    });

    afterEach(() => {
        component.element.remove();
        component = null;
    });

    it('should be initialized', () => {
        expect(component.element).toBeTruthy();
    });

    it('should add email', () => {
        component.addEmail(emailValue);

        const email = component.element.querySelector<HTMLElement>('.' + EmailComponent.hostClass);
        const input = email.querySelector<HTMLInputElement>('.' + EmailComponent.inputClass);

        expect(input.value).toBe(emailValue);
    });

    it('should add emails', () => {
        const check = (emailValues) => {
            component.addEmails(emailValues);

            const emails = component.element.querySelectorAll<HTMLElement>('.' + EmailComponent.hostClass);

            const inputs = [];
            emails.forEach((email) => {
                inputs.push(email.querySelector<HTMLInputElement>('.' + EmailComponent.inputClass));
            });

            const resultEmailValues = Array.from(emails).reduce((resultEmailValues, email) => {
                const input = email.querySelector<HTMLInputElement>('.' + EmailComponent.inputClass);
                resultEmailValues.push(input.value);
                return resultEmailValues;
            }, []);

            if (typeof emailValues === 'string') {
                emailValues = emailValues.split(',');
                emailValues = emailValues.map((value) => value.trim());
            }

            expect(emailValues.every((value) => resultEmailValues.indexOf(value) !== -1)).toBeTruthy();
        };

        check(['test@test.test', 'test2@test.test']);
        check('test@test.test, test2@test.test');
    });

    it('should remove email', () => {
        component.addEmail(emailValue);

        const email = component.element.querySelector<HTMLElement>('.' + EmailComponent.hostClass);

        component.removeEmail(emailValue);

        expect(component.element.contains(email)).toBeFalsy();
    });

    it('should check email exist', () => {
        component.addEmail(emailValue);
        expect(component.emailExists(emailValue)).toBeTruthy();

        component.removeEmail(emailValue);
        expect(component.emailExists(emailValue)).toBeFalsy();
    });

    it('should get valid emails count', () => {
        emails.forEach((email) => component.addEmail(email));
        expect(component.getValidEmailsCount()).toBe(2);
    });

    it('should get emails', () => {
        emails.forEach((email) => component.addEmail(email));
        expect(component.getEmails()).toEqual(emails);
    });

    it('should reset', () => {
        emails.forEach((email) => component.addEmail(email));

        component.reset();
        expect(component.getEmails()).toEqual([]);

        component.reset(emails);
        expect(component.getEmails()).toEqual(emails);
    });

    it('should dispatch emails change event', () => {
        const handlerSpy  = jasmine.createSpy('handlerSpy');

        component.element.addEventListener('emailsChange', handlerSpy);

        component.addEmail(emailValue);
        expect(handlerSpy).toHaveBeenCalled();
        expect(handlerSpy.calls.first().args[0].detail).toEqual([emailValue]);
        handlerSpy.calls.reset();

        component.removeEmail(emailValue);
        expect(handlerSpy).toHaveBeenCalled();
        expect(handlerSpy.calls.first().args[0].detail).toEqual([]);
        handlerSpy.calls.reset();

        component.addEmails(emails);
        expect(handlerSpy).toHaveBeenCalled();
        expect(handlerSpy.calls.first().args[0].detail).toEqual(emails);
        handlerSpy.calls.reset();

        component.reset();
        expect(handlerSpy).toHaveBeenCalled();
        expect(handlerSpy.calls.first().args[0].detail).toEqual([]);

        component.element.removeEventListener('emailsChange', handlerSpy);
    });
});
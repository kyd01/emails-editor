import { Component } from '../component/component.class';
import { EmailComponent } from '../email/email.component';

require('./control.component.sass');

export class ControlComponent extends Component {
    public static readonly hostClass: string = 'emails-control';
    public static readonly newEmailClass: string = EmailComponent.hostClass + '--new';

    public newEmail: EmailComponent;
    public newEmailPlaceholder: string;

    private emails: EmailComponent[] = [];

    public render() {
        this.addClass(ControlComponent.hostClass);

        this.newEmail = new EmailComponent();
        this.newEmail.isEditOnly = true;
        this.newEmail.render();
        this.newEmail.activate();
        this.newEmail.addClass(ControlComponent.newEmailClass);
        this.newEmail.input.element.placeholder = this.newEmailPlaceholder;
        this.element.appendChild(this.newEmail.element);

        this.newEmail.input.onInputEnd = (value) => this.addEmail(value);
        this.newEmail.input.onPaste = this.addEmails.bind(this);

        this.element.onclick = this.onClick.bind(this);
        this.element.onmousedown = this.onMouseDown.bind(this);
    }

    public addEmail(value: string, silent: boolean = false): boolean {
        this.newEmail.input.reset();
        value = value.trim();

        if (!value || this.emailExists(value)) {
            return false;
        }

        const email = new EmailComponent();
        email.render();
        email.value = value;
        email.validate();
        this.emails.push(email);
        this.element.insertBefore(email.element, this.newEmail.element);

        if (!silent) {
            this.dispatchChangeEvent();
        }

        return true;
    }

    public addEmails(values: string[] | string) {
        if (typeof values === 'string') {
            values = values.split(',');
        }

        if (!values.length) {
            return;
        }

        let success: boolean = false;

        values.forEach((value) => {
            const isAdded = this.addEmail(value, true);

            if (isAdded) {
                success = true;
            }
        });

        if (success) {
            this.dispatchChangeEvent();
        }
    }

    public getEmails(): string[] {
        return this.emails.map((email) => email.value);
    }

    public reset(emails: string[] | string = []) {
        while (this.element.firstChild) {
            this.element.removeChild(this.element.firstChild);
        }

        this.element.onclick = null;
        this.element.onmousedown = null;

        this.emails = [];

        this.render();

        if (emails) {
            this.addEmails(emails);
        }

        this.dispatchChangeEvent();
    }

    public emailExists(value: string): boolean {
        return !!this.getEmailComponent(value);
    }

    public removeEmail(value: string) {
        const email = this.getEmailComponent(value);

        if (!email) {
            return;
        }

        email.element.remove();
        this.emails.splice(this.emails.indexOf(email), 1);

        this.dispatchChangeEvent();
    }

    public getValidEmailsCount(): number {
        return this.emails.reduce((count: number, email: EmailComponent) => {
            return email.isValid ? count + 1 : count;
        }, 0);
    }

    private onClick(event: MouseEvent) {
        const target = event.target as HTMLElement;

        const isRemove: boolean = target.classList.contains(EmailComponent.removeClass);

        if (isRemove) {
            const input = target
                .closest('.' + EmailComponent.hostClass)
                .querySelector<HTMLInputElement>('.' + EmailComponent.inputClass);

            this.removeEmail(input.value);
            return;
        }

        const isInput: boolean = target.classList.contains(EmailComponent.inputClass);

        if (!isInput) {
            this.newEmail.input.element.focus();
        }
    }

    private onMouseDown(event: MouseEvent) {
        const target = event.target as HTMLElement;

        const isInput: boolean = target.classList.contains(EmailComponent.inputClass);
        let isNewEmail: boolean = false;
        if (isInput) {
            const emailElement = target.closest('.' + EmailComponent.hostClass);
            isNewEmail = emailElement.classList.contains(ControlComponent.newEmailClass);
        }

        if (isInput && !isNewEmail) {
            this.setActiveEmail((target as HTMLInputElement).value);
        }
    }

    private setActiveEmail(value: string) {
        const email = this.getEmailComponent(value);
        email.activate();

        if (!email) {
            return;
        }

        const previousValue = email.value;

        email.input.onInputEnd = (value: string, isKeyboardEvent: boolean) => {
            email.deactivate();
            email.validate();

            const isDuplicatedEmail = this.emails.filter((email) => email.value === value).length > 1;

            if (!value || isDuplicatedEmail) {
                this.removeEmail(value);
            }

            if (value && previousValue !== value) {
                this.dispatchChangeEvent();
            }

            if (isKeyboardEvent) {
                this.newEmail.input.element.focus();
            }
        };

        email.input.onPaste = (value) => {
            email.deactivate();

            this.removeEmail(email.input.element.value);
            this.addEmails(value);
        };
    }

    private getEmailComponent(value: string): EmailComponent {
        return this.emails.find((email) => email.value === value);
    }

    private dispatchChangeEvent() {
        const event = new CustomEvent('emailsChange', {detail: this.getEmails()});
        this.element.dispatchEvent(event);
    }
}
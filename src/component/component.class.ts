export class Component {
    public element: HTMLElement;

    constructor(tagName: string = 'div') {
        this.element = Component.newElement(tagName);
    }

    public static newElement(tagName: string = 'div'): HTMLElement {
        return document.createElement(tagName);
    }

    public addClass(className: string) {
        this.element.classList.add(className);
    }

    public removeClass(className: string) {
        this.element.classList.remove(className);
    }
}
import { Component } from './component.class';

describe('Component', () => {
    const tagName = 'div';
    let component: Component;

    beforeEach(() => {
        component = new Component(tagName);
    });

    afterEach(() => {
        component.element.remove();
        component = null;
    });

    it('should be initialized', () => {
        expect(component.element.tagName.toLowerCase()).toBe(tagName);
    });

    it('should add/remove class', () => {
        const className = 'test';

        component.addClass(className);
        expect(component.element.classList.contains(className)).toBeTruthy();

        component.removeClass(className);
        expect(component.element.classList.contains(className)).toBeFalsy();
    });

    it('should create element', () => {
        const tagName = 'button';
        const element = Component.newElement(tagName);
        expect(element.tagName.toLowerCase()).toBe(tagName);
    });
});
import { EditorComponent } from './editor/editor.component';
import { Options } from './options.interface';
import { ControlComponent } from './control/control.component';

function create(options: Options): ControlComponent {
    options = Object.assign({}, options);

    const container = options.container;
    const emails = options.emails;

    delete options.container;
    if (options.emails) {
        delete options.emails;
    }

    const editor = new EditorComponent();
    Object.assign(editor.options, options);
    editor.element = container as HTMLElement;
    editor.render();

    if (emails) {
        editor.control.addEmails(emails);
    }

    return editor.control;
}

export {
    create
};
import { EditorOptions } from './editor/editor-options.interface';

export interface Options extends EditorOptions {
    container: Element;
    emails?: string[] | string;
}
import { InputComponent } from './input.component';

describe('InputComponent', () => {
    let component: InputComponent;

    beforeEach(() => {
        component = new InputComponent();
    });

    afterEach(() => {
        component.element.remove();
        component = null;
    });

    it('should be initialized', () => {
        expect(component.element).toBeTruthy();
    });

    it('should reset', () => {
        component.element.value = 'test';
        component.reset();
        expect(component.element.value).toBeFalsy();
    });

    it('should handle input end event', () => {
        const onInputEnd = jasmine.createSpy('onInputEnd');
        const value = 'test';
        component.element.value = value;
        component.onInputEnd = onInputEnd;

        component.element.dispatchEvent(new Event('blur'));
        expect(onInputEnd).toHaveBeenCalledWith(value, false);
        onInputEnd.calls.reset();

        component.element.dispatchEvent(new KeyboardEvent('keydown', {key: 'Enter'}));
        expect(onInputEnd).toHaveBeenCalledWith(value, true);
        onInputEnd.calls.reset();

        component.element.dispatchEvent(new KeyboardEvent('keydown', {key: ','}));
        expect(onInputEnd).toHaveBeenCalledWith(value, true);
    });

    it('should handle paste event', () => {
        const onPaste = jasmine.createSpy('onPaste');
        const clipboardText = 'test';
        const clipboardData = new DataTransfer();
        clipboardData.setData('text', clipboardText);
        component.onPaste = onPaste;

        component.element.dispatchEvent(new ClipboardEvent('paste', {
            clipboardData: clipboardData
        }));
        expect(onPaste).toHaveBeenCalledWith(clipboardText);
    });
});
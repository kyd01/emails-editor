import { Component } from '../component/component.class';

export class InputComponent extends Component {
    public element: HTMLInputElement;

    constructor() {
        super('input');
    }

    public set onInputEnd(handler: (value: string, isKeyboardEvent: boolean) => void) {
        if (!handler) {
            this.element.onblur = null;
            this.element.onkeydown = null;
            this.element.onkeypress = null;
            return;
        }

        this.element.onblur = (event: FocusEvent) => handler((event.target as HTMLInputElement).value, false);

        this.element.onkeydown = (event: KeyboardEvent) => {
            if (['Enter', ','].indexOf(event.key) === -1) {
                return;
            }

            event.preventDefault();
            handler((event.target as HTMLInputElement).value, true);
        };
    }

    public set onPaste(handler: (value: string) => void) {
        if (!handler) {
            this.element.onpaste = null;
            return;
        }

        this.element.onpaste = (event: ClipboardEvent) => {
            event.preventDefault();
            handler(event.clipboardData.getData('text'));
        };
    }

    public reset() {
        this.element.value = '';
    }
}
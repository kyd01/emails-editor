export interface EditorOptions {
    title?: string;
    titleTagName?: string;
    addMoreText?: string;
    addRandomButtonText?: string;
    showValidCountButtonText?: string;
}
import { Component } from '../component/component.class';
import { ControlComponent } from '../control/control.component';
import { EditorOptions } from './editor-options.interface';

require('./editor.component.sass');

export class EditorComponent extends Component {
    public static readonly hostClass: string = 'emails-editor';
    public static readonly titleClass: string = EditorComponent.hostClass + '__title';
    public static readonly controlContainerClass: string = EditorComponent.hostClass + '__control-container';
    public static readonly addRandomClass: string = EditorComponent.hostClass + '__add-random';
    public static readonly showValidCountClass: string = EditorComponent.hostClass + '__show-valid-count';

    public options: EditorOptions = {
        title: 'Share <strong>Board name</strong> with other',
        titleTagName: 'h2',
        addMoreText: 'add more people...',
        addRandomButtonText: 'Add email',
        showValidCountButtonText: 'Get email count'
    };

    public control: ControlComponent;

    public render() {
        this.addClass(EditorComponent.hostClass);

        const controlContainer = Component.newElement();
        controlContainer.classList.add(EditorComponent.controlContainerClass);
        this.element.appendChild(controlContainer);

        const title = Component.newElement(this.options.titleTagName);
        title.innerHTML = this.options.title;
        title.classList.add(EditorComponent.titleClass);
        controlContainer.appendChild(title);

        this.control = new ControlComponent();
        this.control.newEmailPlaceholder = this.options.addMoreText;
        this.control.render();
        controlContainer.appendChild(this.control.element);

        const addRandomButton = Component.newElement('button');
        addRandomButton.innerText = this.options.addRandomButtonText;
        addRandomButton.classList.add(EditorComponent.addRandomClass);
        this.element.appendChild(addRandomButton);

        const showValidCountButton = Component.newElement('button');
        showValidCountButton.innerText = this.options.showValidCountButtonText;
        showValidCountButton.classList.add(EditorComponent.showValidCountClass);
        this.element.appendChild(showValidCountButton);

        addRandomButton.onclick = this.addRandomEmail.bind(this);
        showValidCountButton.onclick = this.showValidEmailsCount.bind(this);
    }

    public addRandomEmail() {
        const email = EditorComponent.getRandomString() + '@' +
            EditorComponent.getRandomString().substring(0, 4) + '.' +
            EditorComponent.getRandomString().substring(0, 2);
        this.control.addEmail(email);
    }

    public showValidEmailsCount() {
        alert(this.control.getValidEmailsCount());
    }

    private static getRandomString(): string {
        return Math.random().toString(36).substring(2, 15).replace(/\d+/g, '');
    }
}
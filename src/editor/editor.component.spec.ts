import { EditorComponent } from './editor.component';
import { EmailComponent } from '../email/email.component';

describe('EditorComponent', () => {
    let component: EditorComponent;

    beforeEach(() => {
        component = new EditorComponent();
        component.render();
    });

    afterEach(() => {
        component.element.remove();
        component = null;
    });

    it('should be initialized', () => {
        expect(component.element).toBeTruthy();
    });

    it('should add random email', () => {
        const getEmailsCount = () => {
            return component.element.querySelectorAll<HTMLElement>('.' + EmailComponent.hostClass).length;
        };

        const countBefore = getEmailsCount();

        component.addRandomEmail();
        component.addRandomEmail();
        component.addRandomEmail();

        expect(getEmailsCount()).toBe(countBefore + 3);

        const invalidCount = component
            .element
            .querySelectorAll<HTMLElement>('.' + EmailComponent.invalidClass)
            .length;

        expect(invalidCount).toBe(0);
    });

    it('should show valid emails count', () => {
        const alertSpy = spyOn(window, 'alert');

        component.addRandomEmail();
        component.addRandomEmail();

        component.showValidEmailsCount();

        expect(alertSpy).toHaveBeenCalledWith(2);
    });
});
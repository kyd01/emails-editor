'use strict';

process.noDeprecation = true;

module.exports = function (env) {
    env = env || {};

    const webpack = require('webpack');

    const UglifyJsPlugin = require( 'uglifyjs-webpack-plugin' );
    const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
    const OptimizeCSSAssetsPlugin = require( 'optimize-css-assets-webpack-plugin' );
    const HtmlWebpackPlugin = require('html-webpack-plugin');

    const path = require('path');
    const include = path.resolve(__dirname, 'src');

    let config = {
        mode: env.production ? 'production' : 'development',
        entry: './src',
        output: {
            filename: 'emails-editor.js',
            library: 'EmailsEditor',
            libraryTarget: 'umd',
            umdNamedDefine: true
        },
        resolve: {
            extensions: ['.ts', '.js']
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    enforce: 'pre',
                    loader: 'tslint-loader'
                },
                {
                    test: /\.ts$/,
                    include: include,
                    loader: 'ts-loader'
                },
                {
                    test: /\.html$/,
                    include: include,
                    loader: 'html-loader'
                },
                {
                    test: /(\.css$|\.s[ac]ss$)/,
                    include: include,
                    use: [
                        env.production ? MiniCssExtractPlugin.loader : 'style-loader',
                        'css-loader',
                        'sass-loader'
                    ]
                }
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                DEBUG: !env.production
            })
        ]
    };

    if (env.production) {
        config.optimization = {
            minimizer: [
                new UglifyJsPlugin({
                    cache: true,
                    parallel: true,
                    sourceMap: false
                }),
                new OptimizeCSSAssetsPlugin()
            ]
        };
        config.plugins.push(new MiniCssExtractPlugin({
            filename: 'emails-editor.css'
        }));
        config.plugins.push(new HtmlWebpackPlugin({
            template: 'index.html',
            includeStyles: true
        }));
    }
    else {
        config.watch = true;
        config.devtool = 'source-map';
        config.devServer = {
            contentBase: './',
            hot: true,
            port: 9000
        };
        config.plugins.push(new HtmlWebpackPlugin({
            template: 'index.html'
        }));
    }

    return config;
};